# Nokia Camera
## Getting Started :
### Clonning :
```
git clone https://gitlab.com/userariii/vendor_nokia_NokiaCamera.git -b main vendor/nokia/NokiaCamera
```
### Changes Required in Device Tree:

• To ship with Nokia Camera in your builds
```
export TARGET_SHIPS_NokiaCam=true
```
OR set in device.mk
```
TARGET_SHIPS_NokiaCam := true
```
• Done, continue building your ROM as you do normally.