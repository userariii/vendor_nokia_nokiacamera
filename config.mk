PRODUCT_SOONG_NAMESPACES += \
    vendor/nokia/NokiaCamera

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/nokia/NokiaCamera/proprietary/system/etc,$(TARGET_COPY_OUT_SYSTEM)/etc) \
    $(call find-copy-subdir-files,*,vendor/nokia/NokiaCamera/proprietary/system/priv-app/Camera/lib,$(TARGET_COPY_OUT_SYSTEM)/priv-app/Camera/lib) \
    $(call find-copy-subdir-files,*,vendor/nokia/NokiaCamera/proprietary/system/priv-app/BokehEditor/lib,$(TARGET_COPY_OUT_SYSTEM)/priv-app/Camera/lib)
    
# Nokia Camera
ifeq ($(TARGET_SHIPS_NokiaCam), true)
PRODUCT_PACKAGES += \
    Camera \
    BokehEditor
endif
